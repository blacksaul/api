//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var bodyparser=require('body-parser');
app.use(bodyparser.json())
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})//cross origin, se le indica para peticiones inseguras deje pasar API Rest

var movimientosV2JSON = require('./movimientos.json');
var requestjson = require('request-json');
var urlClientesMlab = "https://api.mlab.com/api/1/databases/srodriguez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlClientesMlab);




app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send('Hemos recibido su peticion get');
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/',function(req,res){
  res.send('Hemos recibido su peticion post')
})

app.put('/',function(req,res){
  res.send('Hemos recibido su peticion put')
})


app.delete('/',function(req,res){
  res.send('Hemos recibido su peticion delete')
})


app.get('/v1/Clientes/:idcliente', function(req,res){
  res.send('Aqui tiene al cliente numero :' + req.params.idcliente);
})

app.post('/v1/Clientes/:idcliente', function(req,res){
  res.send('Aqui tiene al cliente numero :' + req.params.idcliente);
})

app.put('/v1/Clientes/:idcliente', function(req,res){
  res.send('Aqui tiene al cliente numero :' + req.params.idcliente);
})

app.delete('/v1/Clientes/:idcliente', function(req,res){
  res.send('Aqui tiene al cliente numero :' + req.params.idcliente);
})

app.get('/v1/movimientos', function(req,res){
  res.sendfile('movimientos.json');
})

app.get('/v2/movimientos', function(req,res){
  res.send(movimientosV2JSON);
})

app.get('/v2/movimientos/:index', function(req,res){
  console.log(req.params.index);
  res.send(movimientosV2JSON[req.params.index-1])
})

app.get('/v2/movimientosquery', function(req, res){
  console.log(req.query);
  res.send('recibido');
})


app.post('/v2/movimientos', function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosV2JSON.length + 1;
  movimientosV2JSON.push(nuevo);
  res.send('movimiento dado de alta');
})

app.put('/v2/movimientos', function(req, res){
  res.send('Hemos recibido su peticion de actualizacion de movimientos');
})

app.get('/v3/Clientes', function(req, res){
  clienteMlab.get('', function(err, restM, body) {
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.post('/v3/Clientes', function(req, res){
  clienteMlab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})
